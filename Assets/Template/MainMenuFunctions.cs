﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuFunctions : MonoBehaviour {

    private void Awake()
    {
        bool openingServices = true;
        for(int i = 0; i < SceneManager.sceneCount; i++)
        {
            if(SceneManager.GetSceneAt(i).name == "Services")
            {
                openingServices = false;
            }
        }
        if(openingServices)
        {
            Services.Initialise();
        }

        bool openingEmptyScene = true;
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            if (SceneManager.GetSceneAt(i).name == "Empty Scene")
            {
                openingEmptyScene = false;
            }
        }
        if (openingEmptyScene)
        {
            OpenScene("Empty Scene");
        }
    }

    public void OpenScene(string scene)
    {

        SceneManager.LoadSceneAsync (scene, LoadSceneMode.Additive);
    }

    public void CloseScene(string scene)
    {
        SceneManager.UnloadSceneAsync(scene);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void OpenSceneAndCloseAllOthers (string scene) {

        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            if (SceneManager.GetSceneAt(i).name != "Services" && SceneManager.GetSceneAt(i).name != "Empty Scene")
            {
                Debug.Log("Closing " + SceneManager.GetSceneAt(i).name);
                SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i));
            }
        }
        ScoreManager.Instance.ExitGameScene();
        SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
    }
}

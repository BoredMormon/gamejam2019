﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerChooser : MonoBehaviour {

    [SerializeField] bool WASD;

    private void Start()
    {
        if (WASD)
        {
            HermitCrabController controller = GetComponent<HermitCrabController>();
            controller.horizontalAxisName = "Horizontal";
            controller.verticalAxisName = "Vertical";
            controller.jumpName = "Jump";
        }
        else
        {
            HermitCrabController controller = GetComponent<HermitCrabController>();
            controller.horizontalAxisName = "HorizontalA";
            controller.verticalAxisName = "VerticalA";
            controller.jumpName = "JumpA";
        }
    }
}

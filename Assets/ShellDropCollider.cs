﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellDropCollider : MonoBehaviour {


    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other.attachedRigidbody.name + "has entered shell drop zone");

        HermitCrabController hcc = other.attachedRigidbody.GetComponent<HermitCrabController>();

        if(hcc)
        {
            if(hcc.HasShell())
            {
                hcc.ShellOutOfBounds();
            }
        }
    }
}

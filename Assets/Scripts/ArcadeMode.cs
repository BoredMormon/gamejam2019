﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcadeMode : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(Switcher());
	}

    [SerializeField]
    KeyButtonTranslator translator;

    [SerializeField]
    float timer;

    IEnumerator Switcher()
    {
        while (true)
        {
            yield return new WaitForSeconds(10);
            GetInput[] inputs = FindObjectsOfType<GetInput>();
            foreach (GetInput input in inputs)
            {
                input.positiveKeyCode = translator.GetRandom();
                input.negativeKeyCode = translator.GetRandom();
                input.ResetImages();
            }
            GetComponentInChildren<ParticleSystem>().Emit(100);
        }
    }
}

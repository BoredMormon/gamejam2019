﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour, IGotInput {

    //Public variables to set the maximum and minimum positions of the elevator
    public float minHeight, maxHeight;

    [SerializeField]
    float movementScale;

    [SerializeField]
    Rigidbody2D door;
	
	// Update is called once per frame
	public void KeyPressed (bool open) {
        Vector3 newPosition;

        //Moves elevator in correct direction
        newPosition = door.position + new Vector2(0, open ? movementScale : -movementScale);

        //Stops elevator moving too low
        if (newPosition.y < minHeight + transform.position.y)
        {
            newPosition = new Vector3(newPosition.x, minHeight + transform.position.y, newPosition.z);
        }

        //Stop elevator moving too high
        if (newPosition.y > maxHeight + transform.position.y)
        {
            newPosition = new Vector3(newPosition.x, maxHeight + transform.position.y, newPosition.z);
        }

        door.MovePosition(newPosition);
    }
}

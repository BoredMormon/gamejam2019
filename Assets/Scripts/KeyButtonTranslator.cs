﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class KeyButtonTranslator : ScriptableObject
{
    public List<KeyImageCombo> keyImageList;

    public Sprite GetSprite (KeyCode code)
    {
        foreach (KeyImageCombo keyImage in keyImageList)
        {
            if (keyImage.keycode == code)
            {
                return keyImage.sprite;
            }
        }
        Debug.LogError("Image does not exist for " + code.ToString());
        return null;
    }

    public KeyCode GetRandom()
    {
        return keyImageList[Random.Range(0, keyImageList.Count)].keycode;
    }
}

[System.Serializable]
public struct KeyImageCombo
{
    public KeyCode keycode;
    public Sprite sprite;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Shell : MonoBehaviour {


    Vector3 startPosition;
    Quaternion startRotation;

    public UnityEvent OnCollect;
    public UnityEvent OnDrop;

    [SerializeField]
    Collider2D physicalCollider;

    [SerializeField]
    float maxSpeed;

    Rigidbody2D myRigidbody;

    private bool collectable = true;

    private void Start()
    {
        myRigidbody = physicalCollider.attachedRigidbody;
        startPosition = transform.position;
        startRotation = transform.rotation;
    }

    private void Update()
    {
        //constrain x speed
        if(myRigidbody.velocity.x > maxSpeed)
        {
            myRigidbody.velocity = new Vector2(maxSpeed, myRigidbody.velocity.y);
        }
        if (myRigidbody.velocity.x < -maxSpeed)
        {
            myRigidbody.velocity = new Vector2(-maxSpeed, myRigidbody.velocity.y);
        }

        //cpConstrains y speed
        if (myRigidbody.velocity.y > maxSpeed)
        {
            myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, maxSpeed);
        }
        if (myRigidbody.velocity.y < -maxSpeed)
        {
            myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, - maxSpeed);
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (collectable && other.attachedRigidbody && other.attachedRigidbody.GetComponent<HermitCrabController>() && !other.attachedRigidbody.GetComponent<HermitCrabController>().IsStunned())
        {
            OnCollect.Invoke();
            other.attachedRigidbody.GetComponent<HermitCrabController>().pickedUpShell(this.gameObject);
            physicalCollider.enabled = false;
            collectable = false;
        }
    }

    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.GetComponent<ShellExitTag>())
        {
            ShellOutOfBounds();
        }
    }

    public void LaunchShell(bool launchRight)
    {
        OnDrop.Invoke();
        GetComponent<Rigidbody2D>().AddForce(new Vector2(launchRight ? 50 : -50, 300f));
        StartCoroutine("ShellInvulTimer");
    }

    public void ShellOutOfBounds()
    {
        OnDrop.Invoke();
        transform.position = startPosition;
        transform.rotation = startRotation;
        myRigidbody.velocity = Vector3.zero;
    }

    private IEnumerator ShellInvulTimer()
    {
        float invulTime = 0.15f;
        while(invulTime > 0)
        {
            invulTime -= Time.deltaTime;
            yield return null;
        }
        physicalCollider.enabled = true;
        collectable = true;


    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnParticleSystemDone : MonoBehaviour {

    [SerializeField] UnityEvent onStop;

	void OnParticleSystemStopped()
    {
        onStop.Invoke();
    }
}

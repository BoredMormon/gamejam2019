﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellSpawner : MonoBehaviour {

    [SerializeField]
    GameObject shell;

	public void SpawnShell()
    {
        Transform[] spawnPoints = GetComponentsInChildren<Transform>();
        Vector3 spawnLocation = spawnPoints[Random.Range(1, spawnPoints.Length)].position;
        Instantiate(shell, spawnLocation, Quaternion.identity);
    }
}

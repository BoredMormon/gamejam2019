﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterBottle : MonoBehaviour, IGotInput {

    //Public variable to determine how fast the platform moves
    public float movementScale;

    //Public variables to set the maximum and minimum positions of the water bottle
    public float minX, maxX;

    //Transform object to save cpu
    private Transform myTransform;

    //Saves rigidbody
    private Rigidbody2D myRigidbody;

    //Anything on top of the platform
    private List<Rigidbody2D> attachedRigidBodies;

    //Start function to assign transform
    public void Start()
    {
        myTransform = this.transform;
        myRigidbody = GetComponent<Rigidbody2D>();
        attachedRigidBodies = new List<Rigidbody2D>();
    }

    //Function called to move the elevator. Direction is determined by the movescale
    public void KeyPressed(bool moveRight)
    {
        Vector3 newPosition;

        //Moves elevator in correct direction
        newPosition = myTransform.position + new Vector3(moveRight ? movementScale : -movementScale, 0, 0);

        //Stops elevator moving too low
        if (newPosition.x < minX)
        {
            newPosition = new Vector3(minX, newPosition.y, newPosition.z);
        }

        //Stop elevator moving too high
        if (newPosition.x > maxX)
        {
            newPosition = new Vector3(maxX, newPosition.y, newPosition.z);
        }

        myRigidbody.MovePosition(newPosition);
        for(int i = 0; i < attachedRigidBodies.Count; i++)
        {
            Vector3 newPos = attachedRigidBodies[i].transform.position + (newPosition - myTransform.position);
            attachedRigidBodies[i].transform.position += new Vector3((newPosition - myTransform.position).x, 0, 0);
        }
    }

    //Show movement lines in editor
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, new Vector3(minX, transform.position.y, transform.position.z));
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, new Vector3(maxX, transform.position.y, transform.position.z));
        Gizmos.color = Color.grey;
    }

    [ContextMenu("Set Max X position")]
    void SetMaxHeight()
    {
        maxX = this.transform.position.x;
    }

    [ContextMenu("Set Min X position")]
    void SetMinHeight()
    {
        minX = this.transform.position.x;
    }

    //Attach player to platform to move with it
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.attachedRigidbody != null)
        {
            if(other.attachedRigidbody.GetComponent<HermitCrabController>())
            {
                attachedRigidBodies.Add(other.attachedRigidbody);
            }
        }
    }

    //Remove player from platform
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.attachedRigidbody != null)
        {
            if (other.attachedRigidbody.GetComponent<HermitCrabController>())
            {
                attachedRigidBodies.Remove(other.attachedRigidbody);
            }
        }
    }

}

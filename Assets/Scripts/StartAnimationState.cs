﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAnimationState : StateMachineBehaviour
{
    public Texture2D texture;
    public Texture2D shellTexture;

    // This will be called when the animator first transitions to this state.
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.gameObject.GetComponent<MeshRenderer>().material.SetTexture("_Texture2D", texture);
        Transform shell = animator.transform.Find("Shell");
        if (shell)
        {
            if (shellTexture)
            {
                shell.gameObject.SetActive(true);
                shell.gameObject.GetComponent<MeshRenderer>().material.SetTexture("_Texture2D", shellTexture);
            }
            else
            {
                shell.gameObject.SetActive(false);
            }
        }
    }
}

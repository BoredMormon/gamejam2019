﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManager : Singleton<ScoreManager> {

    [SerializeField]
    Image p1Score, p2Score;

    GameObject player1, player2;

    [SerializeField]
    float maxScore;

    [SerializeField]
    Animator fadeAnimator;

    [SerializeField]
    Animator sparkleShellAnimator;

    [SerializeField]
    AudioSource victoryFanfareSource;

    private int playerWithShell;

    private int player1Rounds, player2Rounds;

    private float player1Score, player2Score;

    private bool levelEnding;

    public GameObject maincam;

    [SerializeField]
    Transform circleZoom;

    [SerializeField]
    Canvas scoreCanvas;

    [SerializeField]
    GameObject scoreTitle;

    [SerializeField]
    TextMeshProUGUI p1RoundText, p2RoundText;

	// Use this for initialization
	void Start () {
        player1Score = 0;
        player2Score = 0;
        playerWithShell = 0;
        levelEnding = false;
        player1Rounds = 0;
        player2Rounds = 0;
	}
	
	// Update is called once per frame
	void Update () {

        switch (playerWithShell)
        {
            case 1: player1Score += Time.deltaTime;
                sparkleShellAnimator.SetFloat("ScoreSpeedMultiplier", (float)(player1Score / maxScore));
                break;
            case 2: player2Score += Time.deltaTime;
                sparkleShellAnimator.SetFloat("ScoreSpeedMultiplier", (float)(player2Score / maxScore));
                break;
            case 0:
                sparkleShellAnimator.SetFloat("ScoreSpeedMultiplier", 0);
                break;
        }
        p1Score.fillAmount = (float)(player1Score / maxScore);
        p2Score.fillAmount = (float)(player2Score / maxScore);
        

        if((player1Score >= maxScore || player2Score >= maxScore) && !levelEnding)
        {
            levelEnding = true;
            StartCoroutine("EndingLevel");
        }

    }

    private IEnumerator EndingLevel()
    {
        AudioSource auds = GetComponent<AudioSource>();
        auds.clip = SoundManager.Instance.GetSound("victoryFanfare");
        auds.volume = 0.2f;
        auds.Play();
        bool player1Wins = player1Score >= maxScore;

        if(player1Wins)
        {
            circleZoom.position =  Camera.main.WorldToScreenPoint(player1.transform.position);

            player1.GetComponent<HermitCrabController>().Victory();
            player1Rounds++; 
        }
        else
        {
            circleZoom.position = Camera.main.WorldToScreenPoint(player2.transform.position);

            player2.GetComponent<HermitCrabController>().Victory();
            player2Rounds++;
        }

        circleZoom.LeanScale(new Vector3(2, 2, 2), 2f);

        while (LeanTween.isTweening(circleZoom.gameObject))
        {
            yield return null;
        }

        scoreTitle.SetActive(true);

        p1RoundText.text = player1Rounds.ToString();
        p2RoundText.text = player2Rounds.ToString();

        //TODO: wait then load new level

        //fadeAnimator.SetTrigger("EndLevel");
    }

    public void playerHasShell(int playerNum, GameObject player)
    {
        if(playerNum == 1)
        {
            player1 = player;
        }
        else if(playerNum == 2)
        {
            player2 = player;
        }
        playerWithShell = playerNum;
        player1Score = 0;
        player2Score = 0;
    }

    public void OpenGameScene()
    {
        player1Score = 0;
        player2Score = 0;
        playerHasShell(0, null);
        levelEnding = false;
        scoreCanvas.enabled = true;
        maincam.SetActive(true);
    }

    public void ExitGameScene()
    {
        circleZoom.localScale = new Vector3(20, 20, 20);
        circleZoom.localPosition = new Vector3(0, 0, 0);
        scoreTitle.SetActive(false);
        scoreCanvas.enabled = false;
        maincam.SetActive(false);
    }
}

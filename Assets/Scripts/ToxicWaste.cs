﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToxicWaste : MonoBehaviour {

    //Area effector
    [SerializeField]
    AreaEffector2D myAreaEffector;

    //Force of the stream
    [SerializeField]
    float forceMagnitude;

    [SerializeField]
    float bubblespreadTime;
    float bubbleCountdown;

    AudioSource auds;
	
	// Update is called once per frame
	void Start () {
        myAreaEffector = GetComponentInChildren<AreaEffector2D>();
        myAreaEffector.forceMagnitude = forceMagnitude;
        myAreaEffector.forceAngle = 90;
        auds = GetComponent<AudioSource>();
        auds.volume = 0.02f;
    }

    private void Update()
    {
        bubbleCountdown -= Time.deltaTime;
        if(bubbleCountdown <= 0)
        {
            auds.clip = SoundManager.Instance.GetSound("bubble");
            auds.Play();
            bubbleCountdown = bubblespreadTime;
        }
    }

}

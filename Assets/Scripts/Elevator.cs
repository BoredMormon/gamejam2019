﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour, IGotInput {

    //Public variable to determine how fast the platform moves
    public float movementScale;

    //Public variables to set the maximum and minimum positions of the elevator
    public float minHeight, maxHeight;

    //Transform object to save cpu
    private Transform myTransform;

    private Rigidbody2D myRigidbody;

    public float squeakSpreadTime;
    float squeakCountdown;

    AudioSource auds;

    //Start function to assign transform
    public void Start()
    {
        myTransform = this.transform;
        myRigidbody = GetComponent<Rigidbody2D>();
        auds = GetComponent<AudioSource>();
        auds.volume = 0.05f;
    }

    private void Update()
    {
        squeakCountdown -= Time.deltaTime;
    }

    //Function called to move the elevator. Direction is determined by the movescale
    public void KeyPressed(bool moveUp)
    {
        Vector3 newPosition;

        //Moves elevator in correct direction
        newPosition = myTransform.position + new Vector3(0, moveUp ? movementScale : -movementScale, 0);

        //Stops elevator moving too low
        if(newPosition.y < minHeight)
        {
            newPosition = new Vector3(newPosition.x, minHeight, newPosition.z);
        }
        else if (newPosition.y > maxHeight) //Stop elevator moving too high
        {
            newPosition = new Vector3(newPosition.x, maxHeight, newPosition.z);
        }
        else
        {
            if (squeakCountdown <= 0)
            {
                auds.clip = SoundManager.Instance.GetSound("squeak");
                auds.Play();
                squeakCountdown = squeakSpreadTime;
            }
        }
   
        myRigidbody.MovePosition(newPosition);
    }

    //Show movement lines in editor
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, new Vector3(transform.position.x, minHeight, transform.position.z));
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, new Vector3(transform.position.x, maxHeight, transform.position.z));
        Gizmos.color = Color.grey;
    }

    [ContextMenu("Set Max Height")]
    void SetMaxHeight()
    {
        maxHeight = this.transform.position.y;
    }

    [ContextMenu("Set Min Height")]
    void SetMinHeight()
    {
        minHeight = this.transform.position.y;
    }
}

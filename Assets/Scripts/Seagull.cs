﻿using System.Collections;
using UnityEngine;
using TMPro;

public class Seagull : MonoBehaviour {

    //Collider for where to drop player and exit to
    [SerializeField]
    Collider2D dropOffZone, exitZone;

    //Bool for if seagull drop height is specific
    [SerializeField]
    bool HeightSpecific;

    //Container for the seagull object
    [SerializeField]
    GameObject seagullObject;

    //Sprite renderer for the trigger zone
    [SerializeField]
    SpriteRenderer triggerZone;

    //How long it takes for the seagull to trigger
    [SerializeField]
    float triggerTime;

    //Counter for how long someone has been in the trigger zone
    private float timeInTrigger;

    //Reference to object being targetted by seagull
    private GameObject seagullTarget;

    //Bool on whether an attack is in progress
    private bool attackInProgress;

    //Start position of seagull
    private Vector3 startPos;

    [SerializeField]
    Transform seagullImage;

	// Use this for initialization
	void Start () {
        attackInProgress = false;
        startPos = seagullObject.transform.position;
        dropOffZone.GetComponent<SpriteRenderer>().enabled = false;
        triggerZone.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(seagullTarget != null && !attackInProgress)
        {
            timeInTrigger += Time.deltaTime;
        }

        if(timeInTrigger >= triggerTime)
        {
            timeInTrigger = 0;
            attackInProgress = true;
            //Check if already targetted by a seagull
            if(seagullTarget.GetComponent<HermitCrabController>() && !seagullTarget.GetComponent<HermitCrabController>().IsTargettedBySeagull())
            {
                seagullTarget.GetComponent<HermitCrabController>().TargettedBySeagull(true);
                StartCoroutine("SeagullAttack");
            }
            else
            {
                seagullTarget = null;
                attackInProgress = false;
            }
        }
	}

    //TODO: use list to allow multiple players to be saved/attacked
    //Recognises when a player enters the trigger zone
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.attachedRigidbody != null)
        {
            if (other.attachedRigidbody.GetComponent<HermitCrabController>() && seagullTarget == null)
            {
                seagullTarget = other.attachedRigidbody.gameObject;
                timeInTrigger = 0;
            }
        }
    }

    //Recognise when player exits trigger zone
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.attachedRigidbody != null)
        {
            if (other.attachedRigidbody.gameObject == seagullTarget)
            {
                seagullTarget = null;
                timeInTrigger = 0;
            }
        }
    }

    private IEnumerator SeagullAttack()
    {
        GameObject attackedTarget = seagullTarget;
        Vector2 dropSpot;

        AudioSource audioSource = GetComponent<AudioSource>();
        if (!audioSource)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }
        audioSource.clip = SoundManager.Instance.GetSound("seagull");
        audioSource.volume = 0.15f;
        audioSource.Play();

        //Pick a random position above dropoff zone to drop target
        float xMin = dropOffZone.bounds.min.x;
        float xMax = dropOffZone.bounds.max.x;

        if(HeightSpecific)
        {
            float yMin = dropOffZone.bounds.min.y;
            float yMax = dropOffZone.bounds.max.y;
            dropSpot = new Vector2(Random.Range(xMin, xMax), Random.Range(yMin, yMax));
        }
        else
        {
            float dropX = Random.Range(xMin, xMax);

            Vector3 flightVector = exitZone.transform.position - attackedTarget.transform.position;
            float xDistance = dropX - attackedTarget.transform.position.x;

            float scale = xDistance / flightVector.x;

            dropSpot = (flightVector * scale) + attackedTarget.transform.position;
        }     

        //Seagull starts flying in
        seagullObject.LeanMove(attackedTarget.transform.position, 1f).setSpeed(7f);
        if(attackedTarget.transform.position.x > seagullObject.transform.position.x)
        {
            seagullImage.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            seagullImage.localScale = new Vector3(1, 1, 1);
        }


        //Wait for it to reach target
        while(LeanTween.isTweening(seagullObject))
        {
            yield return null;
        }

        //Seagull starts flying to drop off with object
        seagullObject.LeanMove(dropSpot, 1f).setSpeed(7f);
        attackedTarget.LeanMove(dropSpot, 1f).setSpeed(7f);
        attackedTarget.GetComponentInChildren<Collider2D>().enabled = false;

        //Drop shell if carrying it
        HermitCrabController hcc = attackedTarget.GetComponent<HermitCrabController>();
        if (hcc)
        {
            if(hcc.HasShell())
            {
                hcc.DropShell();
            }
        }

        if (dropSpot.x > seagullObject.transform.position.x)
        {
            seagullImage.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            seagullImage.localScale = new Vector3(1, 1, 1);
        }


        //Wait to reach drop zone
        while (LeanTween.isTweening(seagullObject))
        {
            yield return null;
        }
        attackedTarget.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        attackedTarget.GetComponentInChildren<Collider2D>().enabled = true;
        attackedTarget.GetComponent<HermitCrabController>().TargettedBySeagull(false);

        //Seagull flies off screen
        seagullObject.LeanMove(exitZone.transform.position, 1f).setSpeed(7f);
        if (exitZone.transform.position.x > seagullObject.transform.position.x)
        {
            seagullImage.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            seagullImage.localScale = new Vector3(1, 1, 1);
        }

        //Wait to finish tween
        while (LeanTween.isTweening(seagullObject))
        {
            yield return null;
        }

        //Reset attack
        seagullObject.transform.position = startPos;
        attackInProgress = false;
        
        if (seagullTarget == attackedTarget)
        {
            seagullTarget = null;
        }



    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentHandler : MonoBehaviour, IGotInput
{

    [SerializeField]
    AreaEffector2D myAreaEffector;

    [SerializeField]
    float forceMagnitude;

    [SerializeField]
    GameObject visulisation;

    bool hasInput;

    AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        myAreaEffector = GetComponentInChildren<AreaEffector2D>();
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = SoundManager.Instance.GetSound("babblingBrook");
        audioSource.volume = 0.05f;
    }


    // Update is called once per frame
    public void KeyPressed(bool left)
    {
        if (left)
        {
            myAreaEffector.forceAngle = 180;
            myAreaEffector.forceMagnitude = forceMagnitude;
            hasInput = true;
            visulisation.transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, 1);

        }
        else
        {
            myAreaEffector.forceAngle = 0;
            myAreaEffector.forceMagnitude = forceMagnitude;
            hasInput = true;
            visulisation.transform.localScale = new Vector3(-Mathf.Abs(transform.localScale.x), transform.localScale.y, 1);
        }
    }


    bool canTurnOff = false;
    void FixedUpdate (){
        canTurnOff = true;
    }


    void LateUpdate ()
    {
        if (!canTurnOff) return;
        if (hasInput)
        {
            if(!audioSource.isPlaying)
            {
                audioSource.Play();
            }
            hasInput = false;
            visulisation.SetActive(true);
        }
        else
        {
            audioSource.Stop();
            myAreaEffector.forceMagnitude = 0;
            visulisation.SetActive(false);
        }
        canTurnOff = false;
    }
}

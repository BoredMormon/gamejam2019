﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GetInput : MonoBehaviour
{

    public KeyCode positiveKeyCode;
    public KeyCode negativeKeyCode;

    [SerializeField] Image positiveText;
    [SerializeField] Image negativeText;

    [SerializeField]
    KeyButtonTranslator translator;


    void OnValidate()
    {
        ResetImages();
    }

    private void Start()
    {
        ResetImages();
    }

    public void ResetImages()
    {
        if (positiveText)
        {
            positiveText.sprite = translator.GetSprite(positiveKeyCode);
        }
        if (negativeText)
        {
            negativeText.sprite = translator.GetSprite(negativeKeyCode);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        positiveText.color = Color.white;
        negativeText.color = Color.white;

        if (Input.GetKey(positiveKeyCode))
        {
            positiveText.color = Color.gray;
        }

        if (Input.GetKey(negativeKeyCode))
        {
            negativeText.color = Color.gray;
        }

        if (Input.GetKey(positiveKeyCode) && Input.GetKey(negativeKeyCode)) return;



        if (Input.GetKey(positiveKeyCode))
        {
            ExecuteEvents.Execute<IGotInput>(gameObject, null, (x, y) => { x.KeyPressed(true); });

        }
        if (Input.GetKey(negativeKeyCode))
        {
            ExecuteEvents.Execute<IGotInput>(gameObject, null, (x, y) => { x.KeyPressed(false); });
        }
    }
}

public interface IGotInput : IEventSystemHandler
{
    void KeyPressed(bool positive);
}


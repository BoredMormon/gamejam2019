﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HermitCrabController : MonoBehaviour {

    //Containers for input axis names
    public string horizontalAxisName, verticalAxisName;
    public string jumpName;

    public Transform rayCastOrigin;
    public float maxSlopeAngle, maxSlopeLength;

    //Container for reference to character controller
    private Rigidbody2D myRigidBody;

    //Container for transform
    private Transform myTransform;

    //Public variable for character move speed
    public float moveSpeed;

    //Public variable for maximum upward speed
    public float maxUpSpeed;

    //Jump force variable
    public float jumpForce;

    //Grounded bool
    private bool grounded;

    //Seagull attack bool
    private bool isTargettedBySeagull;

    //Bool for shell ownership
    private bool hasShell;
    private bool stunned;

    private Animator myAnimator;

    private float stunTime;

    private GameObject shellRef;

    [SerializeField]
    float footstepSpacing;

    private float footStepTimer;

    Vector3 lockPos;

    [SerializeField]
    AudioSource audioSource;

    //Player num variable
    [SerializeField]
    int playerNum;

    AudioSource auds;

    // Use this for initialization
    void Start () {
        myRigidBody = this.GetComponent<Rigidbody2D>();
        myTransform = this.transform;
        myAnimator = GetComponentInChildren<Animator>();
        isTargettedBySeagull = false;
        hasShell = false;
        stunned = false;
        stunTime = 0;
        footStepTimer = 0;
        lockPos = new Vector3(-10000, 0, 0);
        auds = GetComponent<AudioSource>();   
    }

    // Update is called once per frame
    private void Update()
    {
        if (lockPos.x == -10000)
        {
            //countdown for footstep sound
            footStepTimer -= Time.deltaTime;

            //Check for grounded
            //TODO: add raycast to both ends of the hermit crab
            Vector3 leftSide = new Vector3(myRigidBody.GetComponentInChildren<Collider2D>().bounds.min.x, myTransform.position.y, myTransform.position.z);
            Vector3 rightSide = new Vector3(myRigidBody.GetComponentInChildren<Collider2D>().bounds.max.x, myTransform.position.y, myTransform.position.z);
            RaycastHit2D[] hitLeft = Physics2D.RaycastAll(leftSide, Vector2.down, myRigidBody.GetComponentInChildren<Collider2D>().bounds.extents.y + 0.05f);
            RaycastHit2D[] hitRight = Physics2D.RaycastAll(rightSide, Vector2.down, myRigidBody.GetComponentInChildren<Collider2D>().bounds.extents.y + 0.05f);

            List<RaycastHit2D> hitList = new List<RaycastHit2D>();
            hitList.AddRange(hitLeft);
            hitList.AddRange(hitRight);


            RaycastHit2D[] hit = hitList.ToArray();

            //Debug.DrawRay(myTransform.position, new Vector3(0, -(myRigidBody.GetComponentInChildren<Collider2D>().bounds.extents.y + 0.05f), 0), Color.red, 1f);
            if (hit.Length > 0)
            {
                grounded = true;
            }
            else
            {
                grounded = false;
            }

            //Check for landing on enemy
            if (!stunned)
            {
                //Jump on someone check is done just inside the edges of the character to stop running into other players triggering it.
                leftSide = new Vector3(myRigidBody.GetComponentInChildren<Collider2D>().bounds.min.x + 0.05f, myTransform.position.y, myTransform.position.z);
                rightSide = new Vector3(myRigidBody.GetComponentInChildren<Collider2D>().bounds.max.x - 0.05f, myTransform.position.y, myTransform.position.z);
                hitLeft = Physics2D.RaycastAll(leftSide, Vector2.down, myRigidBody.GetComponentInChildren<Collider2D>().bounds.extents.y + 0.05f);
                hitRight = Physics2D.RaycastAll(rightSide, Vector2.down, myRigidBody.GetComponentInChildren<Collider2D>().bounds.extents.y + 0.05f);

                hitList = new List<RaycastHit2D>();
                hitList.AddRange(hitLeft);
                hitList.AddRange(hitRight);


                hit = hitList.ToArray();
                for (int i = 0; i < hit.Length; i++)
                {
                    if (hit[i].collider && hit[i].collider.attachedRigidbody)
                    {
                        HermitCrabController hcc = hit[i].collider.attachedRigidbody.GetComponent<HermitCrabController>();
                        if (hcc && hcc != this && hcc.HasShell())
                        {
                            hcc.DropShell();
                        }
                    }
                }
            }

            //Jump command
            if (grounded && Input.GetButtonDown(jumpName))
            {
                myRigidBody.AddForce(new Vector2(0, jumpForce));
                auds.clip = SoundManager.Instance.GetSound("jump");
                auds.volume = 0.02f;
                auds.Play();
            }


            if (!stunned)
            {
                // Set facing direction of sprite
                if (Input.GetAxis(horizontalAxisName) < 0)
                {
                    transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y,1);
                }
                else if (Input.GetAxis(horizontalAxisName) > 0)
                {
                    transform.localScale = new Vector3(-Mathf.Abs(transform.localScale.x), transform.localScale.y,1);
                }


                // Update Animator
                if (grounded)
                {
                    myAnimator.SetBool("Grounded", true);
                    if (Mathf.Abs(Input.GetAxis(horizontalAxisName)) > 0.2f)
                    {
                        if (Input.GetAxis(horizontalAxisName) < 0)
                        {
                            myAnimator.SetBool("Walking", true);
                        }
                        else if (Input.GetAxis(horizontalAxisName) > 0)
                        {
                            myAnimator.SetBool("Walking", true);
                        }
                        if (footStepTimer <= 0)
                        {
                            AudioSource auds = GetComponent<AudioSource>();
                            auds.clip = SoundManager.Instance.GetSound("step");
                            auds.volume = 0.02f;
                            auds.Play();
                            footStepTimer = footstepSpacing;
                        }
                    }
                    else
                    {
                        myAnimator.SetBool("Walking", false);
                    }
                }
                else
                {
                    myAnimator.SetBool("Grounded", false);
                }

            }
            else
            {
                myAnimator.SetBool("Stunned", true);
            }

            if (stunTime <= 0)
            {
                stunTime = 0;
                stunned = false;
                myAnimator.SetBool("Stunned", false);
            }
            else
            {
                stunTime -= Time.deltaTime;
            }
        }
        else
        {
            this.transform.position = lockPos;
        }

        if(hasShell)
        {
            shellRef.transform.position = this.transform.position;
        }
    }
     

    // Update is called at 60fps
    void FixedUpdate () {
        if (lockPos.x == -10000)
        {
            if (!stunned)
            {

                //Clope angle check to make sure players cant walk up/cling to walls
                Vector3 rightDirection = Quaternion.Euler(0, 0, maxSlopeAngle) * transform.right;
                Vector3 leftDirection = Quaternion.Euler(0, 0, -maxSlopeAngle) * -transform.right;
                Vector3 topSide = new Vector3(myTransform.position.x, myRigidBody.GetComponentInChildren<Collider2D>().bounds.max.y - 0.05f, myTransform.position.z);
                RaycastHit2D[] hitTopRight = Physics2D.RaycastAll(topSide, Vector2.right, myRigidBody.GetComponentInChildren<Collider2D>().bounds.extents.x + 0.05f);
                RaycastHit2D[] hitBottomRight = Physics2D.RaycastAll(rayCastOrigin.position, rightDirection, maxSlopeLength);
                RaycastHit2D[] hitTopLeft = Physics2D.RaycastAll(topSide, Vector2.left, myRigidBody.GetComponentInChildren<Collider2D>().bounds.extents.x + 0.05f);
                RaycastHit2D[] hitBottomLeft = Physics2D.RaycastAll(rayCastOrigin.position, leftDirection, maxSlopeLength);

                List<RaycastHit2D> hitRightList = new List<RaycastHit2D>();
                hitRightList.AddRange(hitTopRight);
                hitRightList.AddRange(hitBottomRight);

                List<RaycastHit2D> hitLeftList = new List<RaycastHit2D>();
                hitLeftList.AddRange(hitTopLeft);
                hitLeftList.AddRange(hitBottomLeft);

                Debug.DrawRay(rayCastOrigin.position, transform.right * maxSlopeLength, Color.black, 0.01f, false);
                Debug.DrawRay(rayCastOrigin.position, rightDirection * maxSlopeLength, Color.red, 0.01f, false);
                Debug.DrawRay(rayCastOrigin.position, -transform.right * maxSlopeLength, Color.black, 0.01f, false);
                Debug.DrawRay(rayCastOrigin.position, leftDirection * maxSlopeLength, Color.red, 0.01f, false);


                RaycastHit2D[] hitLeft = hitLeftList.ToArray();
                RaycastHit2D[] hitRight = hitRightList.ToArray();

                //Update horizontal movement
                //Right
                
                if (Input.GetAxis(horizontalAxisName) > 0)
                {
                    if (hitRight.Length == 0)
                    {
                        myRigidBody.velocity = new Vector2(Input.GetAxis(horizontalAxisName) * moveSpeed, myRigidBody.velocity.y);
                    }
                }
                else //Left
                {
                    if (hitLeft.Length == 0)
                    {
                        myRigidBody.velocity = new Vector2(Input.GetAxis(horizontalAxisName) * moveSpeed, myRigidBody.velocity.y);
                    }
                }
                if (Input.GetAxis(horizontalAxisName) < 1 && Input.GetAxis(horizontalAxisName) > -1)
                {
                    myRigidBody.velocity = new Vector2(0, myRigidBody.velocity.y);
                }

            }
            else
            {
                myRigidBody.velocity = new Vector2(0, myRigidBody.velocity.y);
            }

            //Maximum upward velocity
            if (myRigidBody.velocity.y > maxUpSpeed)
            {
                myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, maxUpSpeed);
            }
        }
    }

    public void TargettedBySeagull(bool isTargetted)
    {
        isTargettedBySeagull = isTargetted;
    }

    public bool IsTargettedBySeagull()
    {
        return isTargettedBySeagull;
    }

    public void pickedUpShell(GameObject shell)
    {
        shellRef = shell;
        ScoreManager.Instance.playerHasShell(playerNum, gameObject);
        hasShell = true;
        myAnimator.SetTrigger("Got shell");
    }

    public bool HasShell()
    {
        return hasShell;
    }

    public void DropShell()
    {
        ScoreManager.Instance.playerHasShell(0, null);
        shellRef.transform.position = this.transform.position;
        shellRef.GetComponent<Shell>().LaunchShell(Random.Range(0, 2) > 0);
        hasShell = false;
        stunned = true;
        stunTime = 1f;
    }

    public void ShellOutOfBounds()
    {
        ScoreManager.Instance.playerHasShell(0, null);
        shellRef.transform.position = this.transform.position;
        shellRef.GetComponent<Shell>().ShellOutOfBounds();
        hasShell = false;
        stunned = true;
        stunTime = 1f;
    }

    public bool IsStunned()
    {
        return stunned;
    }

    public void Victory()
    {
        myAnimator.SetTrigger("Victory");
        lockPos = this.transform.position;
    }
}

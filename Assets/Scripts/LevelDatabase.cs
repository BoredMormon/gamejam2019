﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu()]
public class LevelDatabase : ScriptableObject {

	[SerializeField]
    List <string> scenes;

    public void LoadRandomLevel()
    {
        SceneManager.LoadSceneAsync(scenes[Random.Range(0,scenes.Count)], LoadSceneMode.Additive);
        Debug.Log("loading");
    }
}

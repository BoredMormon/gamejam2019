﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager> {

    public AudioClip victoryFanfare, bgm, seagull, babblingBrook, jump;
    public AudioClip[] step, squeak, bubble;

    private AudioSource audioSource;

    private void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
        audioSource.clip = GetSound("bgm");
        audioSource.volume = 0.1f;
        audioSource.Play();
    }

    public AudioClip GetSound(string soundName)
    {
        AudioClip song = this.GetType().GetField(soundName).GetValue(this) as AudioClip;
        if(song == null)
        {
            AudioClip[] songArray = this.GetType().GetField(soundName).GetValue(this) as AudioClip[];
            song = songArray[Random.Range(0, songArray.Length)];
        }


        return song;
    }

}
